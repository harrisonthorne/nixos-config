{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    # realtime audio
    musnix = {
      url = "github:musnix/musnix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # extra hardware configuration
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    # my stuff
    plymouth-theme-musicaloft-rainbow = {
      url = "git+https://codeberg.org/harrisonthorne/plymouth-theme-musicaloft-rainbow?ref=main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    iosevka-muse = {
      url = "git+https://codeberg.org/harrisonthorne/iosevka-muse?ref=main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    muse-sounds = {
      url = "git+https://codeberg.org/harrisonthorne/muse-sounds";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, musnix, nixos-hardware, plymouth-theme-musicaloft-rainbow, iosevka-muse, muse-sounds }@inputs:
    let
      overlaysModule = { config, pkgs, ... }: {
        nixpkgs.overlays = [
          plymouth-theme-musicaloft-rainbow.overlay
          iosevka-muse.overlay
          muse-sounds.overlay
        ];
      };

      extraCommonModules = [ musnix.nixosModules.musnix overlaysModule ];

      littleponyHardwareModules = with nixos-hardware.nixosModules; [
        common-cpu-amd
        common-gpu-amd
        common-pc-laptop
        common-pc-laptop-hdd
      ];

      # note: we would include common-pc-hdd, but it only sets vm.swappiness to
      # 10, which is overriden by common-pc-ssd, which sets vm.swappiness to 1.
      # swap on ponycastle is currently restricted to the ssd.
      ponycastleHardwareModules = with nixos-hardware.nixosModules; [
        common-cpu-amd
        common-gpu-amd
        common-pc
        common-pc-ssd
      ];
    in
    {
      nixosConfigurations = {
        littlepony = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = extraCommonModules ++ littleponyHardwareModules ++ [ ./laptop ];
        };
        ponycastle = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { flake-inputs = inputs; };
          modules = extraCommonModules ++ ponycastleHardwareModules ++ [ ./desktop ];
        };
      };
    };
}
