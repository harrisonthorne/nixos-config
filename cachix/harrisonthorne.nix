{
  nix.settings = {
    substituters = [
      "https://harrisonthorne.cachix.org"
    ];
    trusted-public-keys = [
      "harrisonthorne.cachix.org-1:hJDxYy9Z6ZYp7SEHK5tyhfQgjAIzXOA1WG4Kr18iPUA="
    ];
  };
}
